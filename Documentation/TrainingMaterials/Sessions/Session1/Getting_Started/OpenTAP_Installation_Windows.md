# Prerequisites

| Visual Studio 2017/2019       | ![GUI](../../../Images/Session1/opentap_installation/image2020-6.png)|
| ------------- |:-------------:|
| **DotNET Framework 4.7.2**    |   |
| **DotNET Core Runtime 2.1**   |   |
| **DotNET Core SDK 2.1**       |   |

NOTE: For developer who wish to use OpenTAP Visual Studio SDK, please ensure that Visual Studio is installed prior to "KS8400A Test Automation Developer's System" software. Otherwise, please reinstall "KS8400A Test Automation Developer's System" after Visual Studio installation.

## Download "KS8400A Test Automation Developer's System" from [OpenTAP](https://opentap.keysight.com)

![GUI](../../../Images/Session1/opentap_installation/image2020-1.png)

## Make Installation

![GUI](../../../Images/Session1/opentap_installation/image2020-2.png)

![GUI](../../../Images/Session1/opentap_installation/gif-1.gif)

## Setup license

From [OpenTAP](https://opentap.keysight.com/), scroll down to "Get a License"

![GUI](../../../Images/Session1/opentap_installation/image2020-3.png)

## Verify the installation and license is working

Launch Editor  Start>Keysight Test Automation.

