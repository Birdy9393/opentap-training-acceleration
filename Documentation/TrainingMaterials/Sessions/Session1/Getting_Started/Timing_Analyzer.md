**Timing Analyzer** operates on test plan log(s) and allows you to visualize how much time it takes to execute each part of a test plan. This gives you the information you need to fine tune the test plan for maximum performance.

If a database results listener is configured, the log file opens automatically when Timing Analyzer is launched. Otherwise, No Data is displayed until a log file is manually opened.

# With configured Database result listener

![GUI](../../../Images/Session1/TimingAnalyzer/gif-1.gif)

# Without configured Database result listener

![GUI](../../../Images/Session1/TimingAnalyzer/gif-2.gif)