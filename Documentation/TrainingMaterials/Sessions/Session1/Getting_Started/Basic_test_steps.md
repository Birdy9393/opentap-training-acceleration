On fresh PathWave Automation Platform install, there are two groups of available test steps:

- Basic Test Steps
- Flow Control Test Steps

# **Basic Test Steps**

# Delay

Delays for a specified amount of time.

![GUI](../../../Images/Session1/TestSteps/image2020-1.png)

# Dialog

Used to interact with the user.

![GUI](../../../Images/Session1/TestSteps/image2020-2.png)

Dialog step prompts a dialog box with OK/Cancel or Yes/No button. User can determine the verdict on each button response and specify the timeout of user response

# Log Output

Outputs a message to the log with a specified severity.

![GUI](../../../Images/Session1/TestSteps/image2020-3.png)

User can specify output message to be included into the test plan log and its severity.

# Run Program

Runs a program, and optionally applies regular expressions (regex) to the output.

User can run any program from here. Here is an example of running "ping localhost"

![GUI](../../../Images/Session1/TestSteps/image2020-4.png)

The log shows the ping result after test finish

![GUI](../../../Images/Session1/TestSteps/image2020-5.png)

# SCPI

Sends a SCPI (Standard Commands for Programmable Instruments) command or query to a SCPI instrument. For queries, it processes the result with regular expressions.

Before running SCPI step, user has to add resource - instrument to be used in the step.

![GUI](../../../Images/Session1/TestSteps/image2020-6.png)

# Time Guard

Tries to forcibly end the execution of a number of child steps after a specified timeout. This is not guaranteed to happen depending on the construction of the child steps

Below example of running a Delay step under Time Guard Step. The Time guard will ensure the child step is running within the time specified.

![GUI](../../../Images/Session1/TestSteps/image2020-7.png)

Time Guard Step Setting

![GUI](../../../Images/Session1/TestSteps/image2020-8.png)

Delay Step Setting

![GUI](../../../Images/Session1/TestSteps/image2020-9.png)

## ***Adding instrument***

1. Click "Add New" button next to Instruments in the resource bar.

![GUI](../../../Images/Session1/TestSteps/image2020-10.png)

2. On Bench setting window, click "+"

![GUI](../../../Images/Session1/TestSteps/image2020-11.png)

3. Add "Generic SCPI Instrument"

![GUI](../../../Images/Session1/TestSteps/image2020-12.png)

4. Fill in instrument address. You may find the instrument's address in the drop down list if you have ever connect the instrument using Keysight IO.

![GUI](../../../Images/Session1/TestSteps/image2020-13.png)