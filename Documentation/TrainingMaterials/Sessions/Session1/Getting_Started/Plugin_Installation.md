Let's try to install a plugin "Demonstration".

# Method 1: Use tap CLI

In Opentap installation folder (TAP_PATH)

**Install Demonstration**

```cs
tap package install Demonstration.TapPackage
```

# Method 2: Download TAP plugin package [website](https://packages.opentap.io/)

- On Windows

            Double click on the Demonstration.<version>.TapPackage to install on Windows only

          or
            "%TAP_PATH%/tap" package install Demonstration.<version>.TapPackage

- On Linux:

            "$TAP_PATH/./tap" package install Demonstration.<version>.TapPackage

# Method 3: Download and Install using OpenTap PackageManager.exe on Windows

1. "%TAP_PATH%/PackageManager.exe"

or

from the Editor > Tools > Package Manager

2. Ensure the TAP package website is in Package repository. Click Settings > Add URL > , Enter URL

Package URL
```cs
http://opentap.keysight.com
```

![GUI](../../../Images/Session1/PluginInstallation/image2020-1.png)

3. Search for "Demonstration" plugin from the package list. Click install.

![GUI](../../../Images/Session1/PluginInstallation/image2020-2.png)