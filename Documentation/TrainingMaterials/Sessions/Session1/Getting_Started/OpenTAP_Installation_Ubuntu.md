# Prerequisites

![GUI](../../../Images/Session1/opentap_installation/image2020-7.png)

## Download and Install "OpenTAP(Linux)

**Download from [OpenTAP](https://opentap.keysight.com/index.html).**

![GUI](../../../Images/Session1/opentap_installation/image2020-4.png)

## Make Installation

- Untar the download package
- Follows the README to make installation
sh INSTALL.sh
- Export TAP_PATH and  license file path

1. cd

2. vi .bashrc

3. Add below two statements at end of file

export TAP_PATH=/home/test/.tap
export LM_LICENSE_FILE=@[license.opentap.keysight.com](https://license.opentap.keysight.com/)

4. save and exit

5. source .bashrc

![GUI](../../../Images/Session1/opentap_installation/gif-2.gif)

## Download and Install Keysight Floating License

Download from [OpenTAP Packages](https://opentap.keysight.com/packages.html).

![GUI](../../../Images/Session1/opentap_installation/image2020-5.png)

## Make Installation

- Navigate to the download file location
- "$TAP_PATH/./tap" package install KeysightFloatingLicensing_Linux.TapPackage

## Verify the OpenTAP
```cs
"$TAP_PATH/./tap" run <testplan>
```
![GUI](../../../Images/Session1/opentap_installation/gif-3.gif)