# OpenTAP Architecture

![OpenTAP Architecute](../../Images/Session1/architecture.png)

Captured from [Pathwave Test Automation High Level Introduction](http://opentap.keysight.com/docs/TAP%20Overview.pdf)

## Project Structure 

1. Plugins in coding environment **all in one project** :
![All In One Project](../../Images/Session1/all_in_one.png)

2. Plugins in coding environment **plugins in individual projects** :
![Individual Projects](../../Images/Session1/individual_project.png)


