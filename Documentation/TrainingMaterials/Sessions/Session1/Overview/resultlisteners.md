# Result Listeners 
Result listener is a kind of plugin that process the stream of data generated during test plan execution.

The build in Result listeners fall into two categories: **Database**, and **Text** , The listeners provide different functionality, mostly related to data storage.

![](../../../Images/Session1/Overview/Result_Listeners/Result.png)

![](../../../Images/Session1/Overview/Result_Listeners/table.png)