# Test plans

The testplan is a test configuration which can be created, edited, executed by using OpenTap Editor.exe, a testplan contains

- A set of TestStep
- External Test Parameters if have
- Instruments/Duts info if have
- Package Dependencies
<br/>

The created testplan is saved in xml file format with extension ***.TapPlan***.

In addition the testplan can be executed by **Editor.exe**,  the **tap.exe** CLI can invoke it as well

**On Window** :  *"%TAP_PATH%/tap" run MyPlan.TapPlan*
<br/>
**On Linux** : *"$TAP_PATH/./tap" run MyPlan.TapPlan*

### Sample TestPlan
![](../../../Images/Session1/Overview/testplan/sample_testplan.gif)
[Example TestPlan file](https://confluence.it.keysight.com/download/attachments/210318359/dmm_psu_with_external_pas.TapPlan?version=1&modificationDate=1589872606018&api=v2)

### Testplan hierarchy
![](../../../Images/Session1/Overview/testplan/tesplan_hierarchy.png)

### Execution Order of TestPlan
![](../../../Images/Session1/Overview/testplan/execution_orderpng.png)