# Executables

| Executable Components        | Location           | Help file	  | Comments |
| ------------- |:-------------:| -----:|-----:|
| **TestPlan Editor**	      | %TAP_PATH%\Editor.exe | %TAP_PATH%\EditorHelp.chm |Valid on Windows OS|
| **PackageManager**     | %TAP_PATH%\PackageManager.exe     |   %TAP_PATH%\EditorHelp.chm |Valid on Windows OS|
| **ResultViewer**	 | %TAP_PATH%\ResultsViewer.exe      |   %TAP_PATH%\ResultsViewerHelp.chm |Valid on Windows OS|
| **RunExplorer**	 | %TAP_PATH%\RunExplorer.exe      |    %TAP_PATH%\RunExplorerHelp.chm |Valid on Windows OS|
| **TimingAnalyzer**	 | %TAP_PATH%\TimingAnalyzer.exe      |   %TAP_PATH%\TimingAnalyzerHelp.chm |Valid for product of KS8400A on Windows only|
| **TAP CLI**	| ![](../../../Images/Session1/Overview/tableWindowLinux.png)      |     |Cross Platform Application|

## Directory in Window
![](../../../Images/Session1/Overview/dir_window.png)

## Editor And Its Components
![](../../../Images/Session1/Overview/editor_base.png)
