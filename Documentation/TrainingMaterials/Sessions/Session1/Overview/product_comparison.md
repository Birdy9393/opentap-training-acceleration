# Product Comparison

![License Type](../../../Images/Session1/Overview/licenses.png)

- **KS8400A**: This includes all the GUIs, which are very helpful for R&D and validation/validation purposes. If you have the need to develop a solution/plugin, develop/edit/debug test plans and inspect/compare results, this is very helpful. KS8400A can of course also be deployed in production / manufacturing, if a customer does not want to develop a custom operator GUI. It can be used for R&D, verification and production.
<br/>
- **CE**: Almost the same as above but without the Timing Analyzer. This is basically intended for non-commercial usage. The detailed terms & conditions are described to the user during installation.
<br/>
- **OpenTAP / KS8000A**: This is the core sequencing engine. It’s useful if you want to create a custom application based on OpenTAP, e.g. operator UI for production, where you want to hide as many details as possible. It’s also useful for cases, where you want to control one/multiple engine(s) using the REST-API. The only difference between OpenTAP and KS8000A is, that OpenTAP is supported by the community while KS8000A is supported by Keysight (KCC, JSD, tap.support@keysight.com, our team). In KS8000A you pay for the support.
<br/>

As example, **Nokia is using KS8400A for R&D** purposes, i.e. it’s used for plugin/solutions development and for pilot products/productions validation. For production Nokia has implemented a custom GUI, which is integrated with Nokia shop floor mgmt. systems. It uses the REST-API (KS8106A) for OpenTAP. They intend to use OpenTAP (not KS8000A) in production. If they need to investigated something further in production, they deploy KS8400A on that station for a short period of time. One of the key focus areas of the custom GUI is that it should be as simple as possible, i.e. operator scans the serial number of the product … and after that behind the scenes the correct test plans is loaded and starts executing. The operator only gets to know a pass/fail verdict.
<br/>
As a conclusion, early in product life cycle **KS8400A is a must**. Later in the cycle **KS8400A or OpenTAP / KS8000A** can be used.