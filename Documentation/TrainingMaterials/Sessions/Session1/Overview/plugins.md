# Plugins

Plugins can be of any type of component which implements or be derived interface of ITapPlugin (***base type for all OpenTAP plugins***). They are **plug and play or detachable**.

![](../../../Images/Session1/Overview/plugins_class.png)