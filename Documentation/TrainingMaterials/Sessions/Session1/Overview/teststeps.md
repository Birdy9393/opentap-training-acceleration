# Test Step
- Encapsulates content of your test.
- e.g. Configurations, measurements, control test execution, open a dialog window, make a web request, launch different program, control other steps' execution.

### OpenTap includes Test Steps

**Basic Steps**

- Delay — Delays for a specified amount of time.
- Dialog — Used to interact with the user.
- Log Output — Outputs a message to the log with a specified severity.
- Run Program — Runs a program, and optionally applies regular expressions (regex) to the output.
- SCPI — Sends a SCPI (Standard Commands for Programmable Instruments) command or query to a SCPI instrument. For queries, it processes the result with regular expressions.
- Time Guard — Tries to forcibly end the execution of a number of child steps after a specified timeout. This is not guaranteed to happen depending on the construction of the child steps. 

**Flow Control Steps**

- If Verdict — Runs its child steps only when the verdict of another step has a specific value.
    - Determined during test plan execution.
    - Test plan's verdict will be the most severe verdict of the test steps.
    - Typical verdict behavior of the parent step is the same as Test plan. e.g. Sequential step. However, parent step has the right to decide the verdict.
    - Among the severities:

        | Severity   |  Verdict	| Description 	|
        |-	|-	|-	|
        | 1 	| NotSet (default) 	| No verdict was set	|
        | 2 	| Pass 	| Step or plan passed |
        | 3     | Inconclusive | Insufficient information to make a decision either way.|
        | 4     | Fail | Step or plan failed.|
        | 5     | Aborted | User aborted test plan |
        |6      | Error | An error occurred. Check session logs for more information.|


- Lock — Locks the execution of child steps based on a specified mutex.
- Parallel — Runs its child steps in parallel in separate threads.
- Repeat — Repeats its child steps either a fixed number of times or until the verdict of a specified step changes to a specified state.
Sequence — Runs its child steps sequentially.
- (Legacy)Sweep Loop — Loops all child steps while sweeping specified parameters/settings on the child steps. Exception: When a child step is a Test Plan Reference, the parameters/settings inside the referenced test plan are NOT swept.
- (Legacy)Sweep Loop (Range) — Loops all child steps while sweeping a parameter/setting over a specified range, with either linear or exponential steps. Exception: When a child step is a Test Plan Reference, the parameter/setting inside the referenced test plan is NOT swept.
- Test Plan Reference — References a test plan from an external file directly, without having to store the test plan as steps in the current test plan.  
- Sweep Parameter - Same as Legacy Sweep Loop step, with exception that the sweeping parameters are specified by child step instead of parent step.
- Sweep Parameter range - Same as Legacy Sweep Loop Range step, with exception that the sweeping parameters are specified by child step instead of parent step.  