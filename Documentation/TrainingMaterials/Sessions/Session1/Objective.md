Objectives
- Basic understanding of the PathWave Test Automation
- OpenTAP Architecture
- Test plan and plugins overview
- Use Editor, Command line action
- Use Results Viewer
- Use Timing Analyzer
- Use Run Explorer

**Reference:** 
- Basic Knowledge
    - [EditorHelp.chm](https://confluence.it.keysight.com/download/attachments/210315500/EditorHelp.chm?version=1&modificationDate=1590125864857&api=v2) under OpenTAP installation folder
    - [Test Automation Platform Home](https://confluence.it.keysight.com/display/testAutomationPlatform/Test+Automation+Platform+Home)

- Source code repos
    - [OpenTAP](https://gitlab.com/OpenTAP/opentap.git)
    - [KS8400A(TAP Core Project))](http://gitlab.it.keysight.com/tap/tap)
    - [OpenTAP Plugin Source code Gitlab](http://gitlab.it.keysight.com/tap-plugins)

- Deep Knowledge
    - [OpenTAP Plugin Knowledge](https://confluence.it.keysight.com/display/tesSG/Knowledge)
    - [OpenTAP Core Engine Knowledge](https://confluence.it.keysight.com/display/tesSG/Knowledge+Base)