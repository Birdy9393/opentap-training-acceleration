**We are going to compare the performance of test plan with the resource connection opened throughout the test plan and without.**

# Install Demonstration plugin (In case you have install)

Referring to [Plugin Installation](../../Session1/Getting_Started/Plugin_Installation.html)

# Run Test plans (Charge and Discharge steps)

1. Add instrument "Demo → Battery Test → Power Analyzer" as shown in the gif in Test plans.

2. Add test steps "Demo → Battery Test → Charge" and "Demo → Battery Test → Discharge".

3. On first attempt, run Test plan.

4. On Second attempt, Click Resource opening button to open the connection and run Test plan again.

![GUI](../../../Images/Session1/homework/image2020-1.png)

# Launch Run Explorer

5. Launch Run Explorer. Tools → Run Explorer

![GUI](../../../Images/Session1/homework/image2021-2.png)

6. At Run Explorer, click on compare log selection mode, select the two results that run in step 3 and 4 as left and right. Click on compare logs

![GUI](../../../Images/Session1/homework/image2021-3.png)

7. Observation

You may observe that these test plans have different starting time.

![GUI](../../../Images/Session1/homework/image2021-4.png)