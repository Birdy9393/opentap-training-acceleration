# Result Listener Plugin

Result listener extends from ResultListener base class.

## Default Implementation

TAP provides the flexibility to store the result in different storage system or format. The following illustrates the methods to be override and they are called in certain order.

![ResultListener](../../../Images/Session2/ResultListener/image.png)