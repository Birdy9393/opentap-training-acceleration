# Objective

- Familiarize with OpenTap Visual Studio SDK in WIndow
- Learn to create simple OpenTAP test step and result listener plugin.
- Learn the default implementation of test step and result listener.

## Reference

- ### Basic:

    - [Understanding in Test Step development](https://docs.opentap.io/Developer%20Guide/Test%20Step/#default-implementation)
    - [Understanding in Result Listener development](https://docs.opentap.io/Developer%20Guide/Result%20Listener/)

- ### More Exercise:

    - [OpenTAP SDK Labs](http://gitlab.it.keysight.com/tap/Documentation/blob/integration/TrainingDocs/PathWave%20Test%20Automation%20Training%204%20-%20Programmer%20Lab/PathWave%20Test%20Automation%20Training%204%20-%20Programmer%20Lab.md)

- ### More Examples:

    - In folder: "%TAP_PATH%\Packages\SDK"

- ### Advance reference:

    - [OpenTAP API](https://confluence.it.keysight.com/download/attachments/210322993/OpenTapApiReference.chm?version=1&modificationDate=1590454108787&api=v2)
