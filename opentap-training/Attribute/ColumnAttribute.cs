﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using OpenTap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Attribute
{
    public enum Bin
    {
        [Display("NotSet", Description: "The bin has not been set.")] NotSet,
        [Display("Bin 1", Description: "Bin1 set.")] Bin1,
        [Display("Bin 2", Description: "Bin2 set.")] Bin2,
        [Display("Bin 3", Description: "Bin3 set.")] Bin3
    }

    [Display("Add Result to Column",
         Groups: new[] { "Training", "Attributes", "Column Attribute" },
         Description: "Shows how an test step setting can be added as a column to the test plan editor.")]
    public class ColumnAttribute : TestStep
    {


        [ColumnDisplayName("Bin")]
        [Browsable(true)]
        public Bin OutBin { get; private set; }

        public override void Run()
        {
            // Generate some random number
            var random = new Random().NextDouble();

            var result = Math.Abs(Math.Sin(random));

           

            /**
             * DISPLAY RESULT AND RANDOM AS LOG
             * InvariantCulture , so that Korean PC culture information will not be taken into consideration..
             * this is also called culture insensitive. Not Required but maybe for training purpose.
             */
            Log.Info("result={0}", random.ToString(CultureInfo.InvariantCulture));

            Log.Info($"random={result.ToString(CultureInfo.InvariantCulture)}");

            if (random < 0.2)
            {
                UpgradeVerdict(Verdict.Fail);
                OutBin = Bin.NotSet;
            }
            else
            {
                if (result < 0.4)
                {
                    OutBin = Bin.Bin1;
                }
                else if (result < 0.6)
                {
                    OutBin = Bin.Bin2;
                }
                else
                {
                    OutBin = Bin.Bin3;
                }
                UpgradeVerdict(Verdict.Pass);
            }
        }
    }
}
