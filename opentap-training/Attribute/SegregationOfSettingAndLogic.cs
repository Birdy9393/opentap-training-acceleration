﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using OpenTap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Attribute
{
    public class UserInterface
    {
        [Unit("Hz")]
        public double Frequency { get; set; }
        [Unit("dBm")]
        [Display("Power Level")]
        public double Power { get; set; }
    }

    [Display("SegregationOfSettingAndLogic", Groups:new[] {"Training", "Attributes", "SegregationOfSettingsAndLogic" }, Description: "Insert a description here")]
    public class SegregationOfSettingAndLogic : TestStep
    {
       
        [Display(Name:"Segregated setting -> UserInterface")]
        [EmbedProperties]
        public UserInterface ui { get;set;} = new UserInterface();

        public SegregationOfSettingAndLogic()
        {
            Log.Info($"Frequency value : {ui.Frequency} Hz, Power: {ui.Power}");
        }

        public override void Run()
        {
            // ToDo: Add test case code.
            RunChildSteps(); 
        }
    }
}
