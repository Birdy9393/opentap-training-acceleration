﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using OpenTap;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ResultPublisher
{

    [Display("Test Measurement")]
    public class MeasurementData
    {
        [Display("Unit")]
        public Int32 UnitNumber { get; set; }

        [Display("Time [S]")]
        public Double MeasurementTime { get; set; }

        [Display("Voltage [mV]")]
        public Double Voltage { get; set; }

        [Display("Current [mA]")]
        public Double Current { get; set; }

        public MeasurementData()
        {
        }
    }


    [Display("Result Publish From Class", Group: "ResultPublisher", Description: "Insert a description here")]
    public class ResulPublishFromClass : TestStep
    {
        #region Settings
        // ToDo: Add property here for each parameter the end user should be able to change
        List<MeasurementData> results = new List<MeasurementData>();
        #endregion

        public ResulPublishFromClass()
        {
            #region mock data
            

            MeasurementData data1 = new MeasurementData()
            {
                UnitNumber = 1,
                MeasurementTime = 4.8,
                Voltage = 5.054434,
                Current = 1.04534
            };

            MeasurementData data2 = new MeasurementData()
            {
                UnitNumber = 2,
                MeasurementTime = 4.5,
                Voltage = 5.8,
                Current = 1.8
            };

            MeasurementData data3 = new MeasurementData()
            {
                UnitNumber = 3,
                MeasurementTime = 4.8,
                Voltage = 5.77,
                Current = 1.063
            };

            MeasurementData data4 = new MeasurementData()
            {
                UnitNumber = 4,
                MeasurementTime = 4.8,
                Voltage = 5.34,
                Current = 1.56
            };

            results.Add(data1);
            results.Add(data2);
            results.Add(data3);
            results.Add(data4);
            #endregion mock data
        }

        public override void Run()
        {
            foreach (var result in results)
            {
                Results.Publish(result);
            }

            // ToDo: Add test case code.
            RunChildSteps(); //If the step supports child steps.

            // If no verdict is used, the verdict will default to NotSet.
            // You can change the verdict using UpgradeVerdict() as shown below.
            // UpgradeVerdict(Verdict.Pass);
        }
    }
}
