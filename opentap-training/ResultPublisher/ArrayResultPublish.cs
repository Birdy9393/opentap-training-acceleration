﻿using OpenTap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResultPublisher
{

        [Display("Store data of Array", Group: "ResultPublisher",
        Description: "This example shows how to store results for XY array with optional limits.")]
        public class ArrayResultPublish : TestStep
        {
            [Display("Point Count", Order: 1, Description: "The number of points to generate.")]
            public int PointCount { get; set; }

            [Display("Enable Limits", Order: 2, Description: "Enables limits to be stored with data.")]
            public bool LimitsEnabled { get; set; }

            public ArrayResultPublish()
            {
                PointCount = 10;
                LimitsEnabled = true;
            }

            public override void Run()
            {

                #region POINT MOCK
                int[] xValues = new int[PointCount];
                double[] yValues = new double[PointCount];


                for (var i = 0; i < PointCount; i++)
                {

                xValues[i] = Math.Abs(new Random().Next() * i);
                    yValues[i] = Math.Abs(new Random().NextDouble());
                }
                #endregion end POINT MOCK

                #region Limit Mock
                double[] yLimitHigh = new double[PointCount];
                double[] yLimitLow = new double[PointCount];
                if (LimitsEnabled)
                {
                    for (var i = 0; i < PointCount; i++)
                    {
                        yLimitHigh[i] = Math.Abs(new Random().NextDouble());
                        yLimitLow[i] = Math.Abs(new Random().NextDouble());
                    }
                }

                #endregion end Limit Mock

                #region PUBLISH DATA
                if (!LimitsEnabled)
                {
                    // Use PublishTable when possible, as it has the highest performance.
                    Results.PublishTable("X versus Y", new List<string> { "X Values", "Y Values" }, xValues, yValues);
                }
                else
                {
                    Results.PublishTable("X versus Y", new List<string> { "X Values", "Y Values", "High Limit", "Low Limit" }, xValues, yValues, yLimitHigh, yLimitLow);
                }
                #endregion end PUBLISH DATA
            }
        }
    
}
