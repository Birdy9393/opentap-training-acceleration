﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace ParentChild
{
    [Display("ChildStep", Group: "ParentChild", Description: "This is a test step that can be attached to any parent step.")]
    public class ChildStep : TestStep
    {
        #region Settings
        
        #endregion

        public ChildStep()
        {
            
        }

        public override void Run()
        {
            
            RunChildSteps(); 
        }
    }

    [AllowAsChildIn(typeof(ParentStepAllowAnyChild))]
    [Display("ChildStep-AllowAsChildIn", Group: "ParentChild", Description: "This is a child step accept only ParentStepAllowAnyChild as parent.")]
    public class ChildStepAllowAsChildIn : TestStep
    {
        #region Settings
        
        #endregion

        public ChildStepAllowAsChildIn()
        {
            var parentInt = GetParent<ParentStepAllowAnyChild>().ParameterInt;
        }

        public override void Run()
        {
            RunChildSteps();
        }
    }
}
