﻿// Author: MyName
// Copyright:   Copyright 2021 Keysight Technologies
//              You have a royalty-free right to use, modify, reproduce and distribute
//              the sample application files (and/or any modified version) in any way
//              you find useful, provided that you agree that Keysight Technologies has no
//              warranty, obligations or liability for any sample application files.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using OpenTap;

namespace ParentChild
{
    [AllowAnyChild]
    [Display("ParentStep-AllowAny", Group: "ParentChild", Description: "This is a parent step of any child")]
    public class ParentStepAllowAnyChild : TestStep
    {
        #region Settings
        public int ParameterInt { get; set; }
        #endregion

        public ParentStepAllowAnyChild()
        {
            
        }

        public override void Run()
        {
            RunChildSteps(); //If the step supports child steps.

            
        }
    }

    [AllowChildrenOfType(typeof(ChildStep))]
    [Display("ParentStep-AllowSpecific", Group: "ParentChild", Description: "This is a parent step of ChildStep")]
    public class ParentStepSpecificChild : TestStep
    {
        #region Settings
        
        #endregion

        public ParentStepSpecificChild()
        {
            var allStepsCount = RecursivelyGetChildSteps(TestStepSearch.All).Count();
        }

        public override void Run()
        {
            
            RunChildSteps(); 
        }
    }
}
