﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTap;   // Use OpenTAP infrastructure/core components (log,TestStep definition, etc)

namespace RunAndLog
{
    [Display("TestStep3", Group: "RunAndLog", Description: "Insert description here")]
    public class TestStep3 : TestStep
    {
        #region Settings
        // ToDo: Add property here for each parameter the end user should be able to change.
        #endregion

        public TestStep3()
        {
            // ToDo: Set default values for properties / settings.
        }
        public override void PrePlanRun()
        {
            base.PrePlanRun();
            Log.Debug($"This is PrePlanRun of {nameof(TestStep3)}");
            // ToDo: Optionally add any setup code this step must run before testplan start.
        }

        public override void Run()
        {
            // ToDo: Add test case code here.
            RunChildSteps(); //If step has child steps.


            // If no verdict is used, the verdict will default to NotSet.
            // You can change the verdict using UpgradeVerdict() as shown below.
            UpgradeVerdict(Verdict.Pass);
        }

        public override void PostPlanRun()
        {
            // ToDo: Optionally add any cleanup code this step needs to run after the 
            // entire testplan has finished.
            base.PostPlanRun();
            Log.Debug($"This is PostPlanRun of {nameof(TestStep3)}");
        }

    }
}

