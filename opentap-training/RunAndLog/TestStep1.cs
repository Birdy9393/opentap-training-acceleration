﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTap;   // Use OpenTAP infrastructure/core components (log,TestStep definition, etc)
using TraceSource = global::OpenTap.TraceSource;
namespace RunAndLog
{
    [Display("TestStep1", Group: "RunAndLog", Description: "Insert description here")]
    public class TestStep1 : TestStep
    {
        #region Settings
        // ToDo: Add property here for each parameter the end user should be able to change.
        #endregion
        private global::OpenTap.TraceSource log;

        public TestStep1()
        {
            // ToDo: Set default values for properties / settings.
        }
        public override void PrePlanRun()
        {
            base.PrePlanRun();
            // ToDo: Optionally add any setup code this step must run before testplan start.

            //create your own log
            this.log = global::OpenTap.Log.CreateSource("MyLog");

            //Different types of log messages
            Log.Debug($"This is PrePlanRun of {nameof(TestStep1)}");
            Log.Info("Info Log");
            Log.Error("Error Log");
            Log.Warning("Warning Log");
            
        }

        public override void Run()
        {
            // ToDo: Add test case code here.
            RunChildSteps(); //If step has child steps.


            // If no verdict is used, the verdict will default to NotSet.
            // You can change the verdict using UpgradeVerdict() as shown below.
            UpgradeVerdict(Verdict.Pass);
        }

        public override void PostPlanRun()
        {
            // ToDo: Optionally add any cleanup code this step needs to run after the 
            // entire testplan has finished.
            base.PostPlanRun();
            Log.Debug($"This is PostPlanRun of {nameof(TestStep1)}");
        }
     
    }
}
